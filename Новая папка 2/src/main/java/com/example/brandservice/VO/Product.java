package com.example.brandservice.VO;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {


    private Integer id;
    private String name;
    private String description;
    private String image;
    private int quantity;
    private float price;

}
