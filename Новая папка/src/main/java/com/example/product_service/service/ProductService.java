package com.example.product_service.service;

import com.example.product_service.entity.Product;
import com.example.product_service.repo.ProductRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class ProductService {

    @Autowired
    private final ProductRepository repository;

    public Product saveProduct(Product product) {
        log.info("qwerty");
        return repository.save(product);
    }

    public Product findProductById(Integer id) {
        log.info("qwerty");
        return repository.findByProductId(id);
    }
}
