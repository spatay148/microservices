package com.example.product_service.controller;

import com.example.product_service.entity.Product;
import com.example.product_service.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
@Slf4j
public class ProductController {
    @Autowired
    private ProductService productService;

    @PostMapping("/")
    public Product saveProduct( @RequestBody Product product){
        log.info("qwerty");
        return productService.saveProduct(product);
    }

    @GetMapping("/{id}")
    public Product getOneById(@PathVariable Integer id){
        log.info("qwerty");
       return productService.findProductById(id);
    }
}
